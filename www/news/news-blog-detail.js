define(['text!../news/news-blog-detail.html', "../base/openapi", '../base/util', '../base/components/footer-menu-detail', '../base/bigImg/bigImg'],
	function(viewTemplate, OpenAPI, Util, MenuDetail, BigImg) {
		return Piece.View.extend({
			menu: null,
			id: 'news_news-detail',
			bigImg: null,
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},

			events: {
				"click .refreshBtn": "refreshNewsContent",
				"click .homeBtn": "goBackToHome",
				"click .newsListUer": "gotoUser",
				"click .icon-comment-alt ": "gotoComment",
				"click .tweetContent": "goToReply",
				"click .tweetListImg": 'goToUserInfor',
				"click img": "bigImg"

			},
			bigImg:function(el) {
				Util.bigImg(el);
			},
			goToUserInfor: function(imgEl) {
				//记录从新闻详情还是从评论列表跳转到用户界面，转标志navigateFlag为1
				// Piece.Session.saveObject("navigateFlag",1);
				var json = {
					time: new Date().getTime()
				};
				var url = window.location.href + "&navigateFlag=1";
				window.history.replaceState(json, "", url);

				Util.imgGoToUserInfor(imgEl);
			},

			goToReply: function(el) {
				//记录从新闻详情还是从评论列表跳转到用户界面，转标志navigateFlag为1
				// Piece.Session.saveObject("navigateFlag", 1);
				var json = {
					time: new Date().getTime()
				};
				var url = window.location.href + "&navigateFlag=1";
				window.history.replaceState(json, "", url);

				var $target = $(el.currentTarget);
				var tweetId = Util.request("id");
				var commentAuthorId = $target.attr("data-commentAuthorId");
				var commentId = $target.attr("data-commentId");

				var dataSource = "cube-list-blog_comment_list";
				var fromType = Util.request("fromType");
				var catalog = 3;
				switch (fromType) {
					case "4":
						{
							catalog = 1;
							break;
						}
					case "3":
						{
							catalog = "blog";
							break;
						}
					case "2":
						{
							catalog = 2;
							break;
						}
				}
				this.navigateModule("common/comment-reply?tweetId=" + tweetId + "&commentAuthorId=" + commentAuthorId + "&commentId=" + commentId + "&dataSource=" + dataSource + "&catalog=" + catalog, {
					trigger: true
				});
			},
			gotoUser: function(el) {
				//如果fromUserInfo未清零，那么navigateFlag不清零
				/*if(Piece.Session.loadObject("fromUserInfo") === 0) {
					//从详情页跳到个人信息页，转标志navigateFlag为0
				Piece.Session.saveObject("navigateFlag", 0);
				}*/
				var json = {
					time: new Date().getTime()
				};
				var url = window.location.href + "&navigateFlag=0";
				window.history.replaceState(json, "", url);


				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var author = $('.newsListUer').text();
				author = encodeURI(author);
				var authorid = $('.newsListUer').attr("data-authorid");
				var fromType = Util.request("fromType");
				//用于  用户信息模块返回参数	
				var infoFrom = Util.request("from");

				//从用户信息，，，保留的参数，用于页脚
				var fromType = Util.request("fromType");
				var checkDetail = Util.request("checkDetail");
				var com = Util.request("com");


				this.navigateModule("common/common-seeUser?id=" + id + "&fromAuthor=" + author + "&fromAuthorId=" + authorid + "&fromType=" + fromType + "&from=" + infoFrom + "&from=" + infoFrom + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com, {
					trigger: true
				});

			},
			refreshNewsContent: function() {
				// 删除缓存，再重新加载
				Util.cleanDetailSession();
				this.loadNewsBlogDetail();
				//刷新拿最新的评论数
				this.newCommentContent();
				//刷新时，从服务器拿评论列表
				this.reCommentList();

			},

			goBackToHome: function() {
				//如果fromUserInfo未清零，那么navigateFlag不清零
				/*if(Piece.Session.loadObject("fromUserInfo") === 0) {
					//从详情页跳到个人信息页，转标志navigateFlag为0
				Piece.Session.saveObject("navigateFlag", 0);
				}
				
				//fromUserInfo清零
				Piece.Session.saveObject("fromUserInfo",0);*/

				window.history.back();
			},

			onShow: function() {
				bigImg = new BigImg();
				var me = this;
				var footerTemplate = $(me.el).find("#footerTemplate").html();
				console.info(footerTemplate + "===============0");

				var footerHtml = _.template(footerTemplate, {
					"checkdatail": Util.request("checkDetail"),
					"id": Util.request("id"),
					"url": "news/news-blog-detail",
					"activeIndex": 1
				});
				$("body").append(footerHtml);
				menu = new MenuDetail({
					"parentEl": me,
					"commentListId": "blog_comment_list",
					"apiName": "blog_comment_list"
				});

				/*var fromUserInfo = Piece.Session.loadObject("fromUserInfo");
				if (fromUserInfo === 1) {
					$('#blog_comment_list').parent().hide();
				} else {
					//判断是不是从评论页面进入用户界面返回，navigateFlag是不是为1
					var navigateFlag = Piece.Session.loadObject("navigateFlag");
					if (navigateFlag === 1) {
						$(".footer-menu-detail").find("li").removeClass("active").eq(2).addClass("active");
						$('.content').find('.newsList').hide();
						$('#blog_comment_list').parent().show();

					} else {
						$('#blog_comment_list').parent().hide();
					}
				}*/
				var navigateFlag = Util.request("navigateFlag");
				if (navigateFlag == 1) {
					$(".footer-menu-detail").find("li").removeClass("active").eq(2).addClass("active");
					$('.content').find('.newsList').hide();
					$('#blog_comment_list').parent().show();

				} else {
					$('#blog_comment_list').parent().hide();
				}


				// $('#blog_comment_list').parent().hide();
				// 同时加载详情与评论列表
				this.loadNewsBlogDetail();
				this.loadDetailComment();


			},

			loadNewsBlogDetail: function() {

				var me = this;
				var token = Piece.Store.loadObject("user_token");
				if (token === null) {
					// ----判断是否有未登陆时的缓存-----
					var detailID = Util.request("id");
					var SessionDetail = Piece.Session.loadObject(detailID)
					if (SessionDetail != null) {
						var SessionID = SessionDetail.id;
					}
					// -------------------
					// 判断是否有未登陆时的缓存就拿缓存，根据ID判断，否则重新请求
					if (SessionDetail != null && SessionID == detailID) {
						// 拿缓存数据
						var SessionDetail = Piece.Session.loadObject(detailID);
						SessionDetail.body = Util.removeImg(SessionDetail.body);
						var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
						var newsDetailHtml = _.template(newsDetailTemplate, SessionDetail);
						$(".newsList").remove();
						$(".content").append(newsDetailHtml);
						if (SessionDetail.commentCount != 0) {
							$('.commentImg').css("display", "block");
							$('.commentCount').html(SessionDetail.commentCount)
						}
						if (SessionDetail.commentCount >= 10) {
							$('.promptInformation').css("width", "16px")
						}
						if (SessionDetail.commentCount >= 100) {
							$('.promptInformation').css("width", "22px")
						}


					} else {
						Util.Ajax(OpenAPI.news_blog_detail, "GET", {
							id: Util.request("id"),
							dataType: 'jsonp',
						}, 'json', function(data, textStatus, jqXHR) {
							console.info(data)
							//保存未登陆时的detail缓存
							var NoLandingCache = data.id;
							Piece.Session.saveObject(NoLandingCache, data);

							var dataTemplate = data;
							dataTemplate.body = Util.removeImg(dataTemplate.body);
							var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
							var newsDetailHtml = _.template(newsDetailTemplate, dataTemplate);
							$(".newsList").remove();
							$(".content").append(newsDetailHtml);
							if (data.commentCount != 0) {
								$('.commentImg').css("display", "block");
								$('.commentCount').html(data.commentCount)

							}
							if (data.commentCount >= 10) {
								$('.promptInformation').css("width", "16px")
							}
							if (data.commentCount >= 100) {
								$('.promptInformation').css("width", "22px")
							}
							// 刷新时，重新请求，判断如果是在网友评论页面，则再次隐藏DETAIL
							var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-file-text-alt");
							if (nowPage) {
								$('#blog_comment_list').parent().hide();
								$('.newsList').show();
							} else {
								$('.content').find('.newsList').hide();
								$('#blog_comment_list').parent().show();
							}
							//保存未登陆时的detail缓存
							// var NoLandingCache = data.id;
							// Piece.Session.saveObject(NoLandingCache, data);
						}, null, null);
					}

				} else {
					// 登陆时保存不同的缓存
					// ----判断是否有登陆时的缓存-----
					var detailID = Util.request("id");
					var SessionDetail = Piece.Session.loadObject("L" + detailID)

					if (SessionDetail != null) {
						var SessionID = SessionDetail.id;
					}
					// -------------------
					if (SessionDetail != null && SessionID == detailID) {
						// 拿缓存数据
						SessionDetail.body = Util.removeImg(SessionDetail.body);
						var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
						var newsDetailHtml = _.template(newsDetailTemplate, SessionDetail);
						$(".newsList").remove();
						$(".content").append(newsDetailHtml);
						var favorite = SessionDetail.favorite;


						if (favorite === 1) {
							$(".tab-item").find('.collect').removeClass("icon-star-empty").addClass("icon-star");
							$('.icon-star').css("color", "#0882f0");
						}

						if (SessionDetail.commentCount != 0) {
							$('.commentImg').css("display", "block");
							$('.commentCount').html(SessionDetail.commentCount)

						}
						if (SessionDetail.commentCount >= 10) {
							$('.promptInformation').css("width", "16px")
						}
						if (SessionDetail.commentCount >= 100) {
							$('.promptInformation').css("width", "22px")
						}

					} else {
						// 否则，没有缓存就重新请求
						var accesstoken = token.access_token;
						Util.Ajax(OpenAPI.news_blog_detail, "GET", {
							id: Util.request("id"),
							dataType: 'jsonp',
							access_token: accesstoken
						}, 'json', function(data, textStatus, jqXHR) {
							// 保存登陆时的detail缓存
							var LandingCache = "L" + data.id;
							Piece.Session.saveObject(LandingCache, data);
							
							var dataTemplate = data;
							dataTemplate.body = Util.removeImg(dataTemplate.body);
							var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
							var newsDetailHtml = _.template(newsDetailTemplate, dataTemplate);
							$(".newsList").remove();
							$(".content").append(newsDetailHtml);
							var favorite = data.favorite;
							if (favorite === 1) {
								$(".tab-item").find('.collect').removeClass("icon-star-empty").addClass("icon-star");
								$('.icon-star').css("color", "#0882f0");
							}

							if (data.commentCount != 0) {
								$('.commentImg').css("display", "block");
								$('.commentCount').html(data.commentCount)

							}
							if (data.commentCount >= 10) {
								$('.promptInformation').css("width", "16px")
							}
							if (data.commentCount >= 100) {
								$('.promptInformation').css("width", "22px")
							}
							// 刷新时，重新请求，判断如果是在网友评论页面，则再次隐藏DETAIL
							var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-file-text-alt");
							if (nowPage) {
								$('#blog_comment_list').parent().hide();
								$('.newsList').show();
							} else {
								$('.content').find('.newsList').hide();
								$('#blog_comment_list').parent().show();
							}
							// 保存登陆时的detail缓存
							// var LandingCache = "L" + data.id;
							// Piece.Session.saveObject(LandingCache, data);
						}, null, null);
					}
					//--------------------------
				}

				//如果是在评论页面，那么详情hide
				var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-file-text-alt");
				if (nowPage) {
					$('#blog_comment_list').parent().hide();
					$('.newsList').show();
				} else {
					$('.content').find('.newsList').hide();
					$('#blog_comment_list').parent().show();
				}

			},
			loadDetailComment: function() {
				// 拿缓存中的cube-list-comment-list-params中的ID与新ID比较，判断是否拿缓存
				var id = Util.request("id");
				Util.loadDifferentCommentList(this, "blog_comment_list", OpenAPI.blog_comment_list, {
					'id': id,
					'catalog': Util.request("com"),
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, "-blog_comment_list-params", id);
			},
			// 刷新按钮，点击后从服务器重新拿评论列表数据
			reCommentList: function() {
				// $("#blog_comment_list").remove();
				var id = Util.request("id");
				Util.loadList(this, "blog_comment_list", OpenAPI.blog_comment_list, {
					'id': id,
					'catalog': Util.request("com"),
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, true);
				// console.info(1111111111111111);
				// 刷新时，重新请求，判断如果是在网友评论页面，则再次隐藏DETAIL
				var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-file-text-alt");
				if (nowPage) {
					$('#comment-list').parent().hide();
					$('.newsList').show();
				} else {
					$('.content').find('.newsList').hide();
					$('#comment-list').parent().show();
				}

			},
			// 刷新按钮，加载最新的评论数信息
			newCommentContent: function() {
				Util.Ajax(OpenAPI.news_blog_detail, "GET", {
					id: Util.request("id"),
					dataType: 'jsonp',
				}, 'json', function(data, textStatus, jqXHR) {
					if (data.commentCount != 0) {
						$('.commentImg').css("display", "block");
						$('.commentCount').html(data.commentCount)
					}
					if (data.commentCount >= 10) {
						$('.promptInformation').css("width", "16px")
					}
					if (data.commentCount >= 100) {
						$('.promptInformation').css("width", "22px")
					}
				}, null, null);
			}
		}); //view define

	});