define(['text!../news/news-detail.html', "../base/openapi", '../base/util', '../base/components/footer-menu-detail', '../base/bigImg/bigImg'],
	function(viewTemplate, OpenAPI, Util, MenuDetail, BigImg) {
		return Piece.View.extend({
			menu: null,
			id: 'news_news-detail',
			bigImg: null,
			listIscroll: null,

			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},

			events: {
				"click .refreshBtn": "refreshNewsContent",
				"click .homeBtn": "goBackToHome",
				// "click .newRelativiesUrl": "goToNewsDetail",
				"click .newsListUer": "gotoUser",
				"click .tweetContent": "goToReply",
				"click .tweetListImg": "goToUserInfor",
				"click img": "bigImg"

			},
			bigImg: function(el) {
				Util.bigImg(el);
			},
			goToUserInfor: function(imgEl) {
				//记录从新闻详情还是从评论列表跳转到用户界面
				// Piece.Session.saveObject("navigateFlag", 1);
				var json = {
					time: new Date().getTime()
				};
				var url = window.location.href + "&navigateFlag=1";
				window.history.replaceState(json, "", url);

				Util.imgGoToUserInfor(imgEl);

			},
			goToReply: function(el) {
				//记录从新闻详情还是从评论列表跳转到用户界面
				// Piece.Session.saveObject("navigateFlag", 1);
				console.info(el.target)
				var json = {
					time: new Date().getTime()
				};
				var url = window.location.href + "&navigateFlag=1";
				window.history.replaceState(json, "", url);

				var $target = $(el.currentTarget);
				var tweetId = Util.request("id");
				var commentAuthorId = $target.attr("data-commentAuthorId");
				var commentId = $target.attr("data-commentId");

				var dataSource = "cube-list-comment-list";
				var fromType = Util.request("fromType");
				var catalog = 3;
				switch (fromType) {
					case "4":
						{
							catalog = 1;
							break;
						}
					case "3":
						{
							catalog = "blog";
							break;
						}
					case "2":
						{
							catalog = 2;
							break;
						}
				}
				this.navigateModule("common/comment-reply?tweetId=" + tweetId + "&commentAuthorId=" + commentAuthorId + "&commentId=" + commentId + "&dataSource=" + dataSource + "&catalog=" + catalog, {
					trigger: true
				});
			},
			gotoUser: function(el) {
				//如果fromUserInfo未清零，那么navigateFlag不清零
				/*if(Piece.Session.loadObject("fromUserInfo") === 0) {
					//从详情页跳到个人信息页，转标志navigateFlag为0
				Piece.Session.saveObject("navigateFlag", 0);
				}*/
				var json = {
					time: new Date().getTime()
				};
				var url = window.location.href + "&navigateFlag=0";
				window.history.replaceState(json, "", url);


				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var author = $('.newsListUer').text();
				author = encodeURI(author);
				var authorid = $('.newsListUer').attr("data-authorid");
				var fromType = Util.request("fromType");
				//用于  用户信息模块返回参数	
				var infoFrom = Util.request("from");

				//从用户信息，，，保留的参数，用于页脚
				var checkDetail = Util.request("checkDetail");
				var com = Util.request("com");


				this.navigateModule("common/common-seeUser?id=" + id + "&fromAuthor=" + author + "&fromAuthorId=" + authorid + "&fromType=" + fromType + "&from=" + infoFrom + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com, {
					trigger: true
				});



			},
			goToNewsDetail: function(el) {
				var $target = $(el.currentTarget);
				var url = $target.attr("data-url");

				var u = Util.getIdFromUrl("news", url);


				var author = $('.newsListUer').text();
				var authorid = $('.newsListUer').attr("data-authorid");
				var fromType = Util.request("fromType");
				//用于  用户信息模块返回参数	
				var infoFrom = Util.request("from");

				//从用户信息，，，保留的参数，用于页脚
				var checkDetail = Util.request("checkDetail");
				var com = Util.request("com");

				this.navigate("news-detail?id=" + u + "&fromAuthor=" + author + "&fromAuthorId=" + authorid + "&fromType=" + fromType + "&from=" + infoFrom + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com, {
					trigger: true
				});

			},
			refreshNewsContent: function() {
				// 删除缓存，再重新加载
				Util.cleanDetailSession();
				this.loadNewsDetail();
				//刷新拿最新的评论数
				this.newCommentContent();

				//刷新时，从服务器拿评论列表
				this.reCommentList();

			},

			goBackToHome: function() {
				//如果fromUserInfo未清零，那么navigateFlag不清零
				/*if(Piece.Session.loadObject("fromUserInfo") === 0) {
					//从详情页跳到个人信息页，转标志navigateFlag为0
				Piece.Session.saveObject("navigateFlag", 0);
				}
				
				//fromUserInfo清零
				Piece.Session.saveObject("fromUserInfo",0);*/
				window.history.back();

			},

			onShow: function() {
				bigImg = new BigImg();
				var id = Util.request("id");
				Piece.Store.saveObject("tempID", id);
				// console.info("================");
				// console.info($(this.el).find(".content .newsList").html());
				// $(this.el).find(".content .newsList").remove();
				var me = this;
				var footerTemplate = $(me.el).find("#footerTemplate").html();

				var footerHtml = _.template(footerTemplate, {
					"checkdatail": Util.request("checkDetail"),
					"id": Util.request("id"),
					"url": "news/news-detail",
					"activeIndex": 1
				});
				$("body").append(footerHtml);
				me.listIscroll = me.components['comment-list'].iScroll;
				
				menu = new MenuDetail({
					"parentEl": me,
					"commentListId": "comment-list",
					"apiName": "comment_list"
				});
				//fromUserInfo清零
				// Piece.Session.saveObject("fromUserInfo",0);
				var navigateFlag = Util.request("navigateFlag");
				// console.info("============navigateFlag==============");
				// console.info( navigateFlag);
				if (navigateFlag == 1) {
					$(".footer-menu-detail").find("li").removeClass("active").eq(2).addClass("active");
					$('.newsList').hide();
					$('#comment-list').parent().show();

				} else {

					$('#comment-list').parent().hide();
				}

				// 同时加载详情与评论列表
				this.loadNewsDetail();
				this.loadDetailComment();

			},
			loadNewsDetail: function() {
				// $(".newsList").remove();
				var me = this;
				var token = Piece.Store.loadObject("user_token");
				if (token === null) {
					// ----判断是否有未登陆时的缓存-----
					var detailID = Util.request("id");
					var SessionDetail = Piece.Session.loadObject(detailID)
					if (SessionDetail != null) {
						var SessionID = SessionDetail.id;
					}
					// -------------------
					// 判断是否有未登陆时的缓存就拿缓存，根据ID判断，否则重新请求
					if (SessionDetail != null && SessionID == detailID) {
						// 拿缓存数据
						var SessionDetail = Piece.Session.loadObject(detailID);
						var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
						SessionDetail.body = Util.removeImg(SessionDetail.body);
						var newsDetailHtml = _.template(newsDetailTemplate, SessionDetail);
						$(".newsList").remove();
						// newsDetailHtml = me.getimgsrc(newsDetailHtml);
						$(".scrollContent").append(newsDetailHtml);
						// $("img").remove();

						if (SessionDetail.commentCount != 0) {
							$('.commentImg').css("display", "block");
							$('.commentCount').html(SessionDetail.commentCount)
						}
						if (SessionDetail.commentCount >= 10) {
							$('.promptInformation').css("width", "16px")
						}
						if (SessionDetail.commentCount >= 100) {
							$('.promptInformation').css("width", "22px")
						}



					} else {
						Util.Ajax(OpenAPI.news_detail, "GET", {
							id: Util.request("id"),
							dataType: 'jsonp',
						}, 'json', function(data, textStatus, jqXHR) {
							console.info(data);
							// alert(data.id);
							console.info(data.id);
							//保存未登陆时的detail缓存
							var NoLandingCache = data.id;
							Piece.Session.saveObject(NoLandingCache, data);

							var dataTemplate = data;
							dataTemplate.body = Util.removeImg(dataTemplate.body);
							var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
							var newsDetailHtml = _.template(newsDetailTemplate, dataTemplate);
							$(".newsList").remove();
							// newsDetailHtml = Util.removeImg(newsDetailHtml);
							$(".scrollContent").append(newsDetailHtml);
							// $("img").remove();
							if (data.commentCount != 0) {
								$('.commentImg').css("display", "block");
								$('.commentCount').html(data.commentCount)

							}
							if (data.commentCount >= 10) {
								$('.promptInformation').css("width", "16px")
							}
							if (data.commentCount >= 100) {
								$('.promptInformation').css("width", "22px")
							}
							// 刷新时，重新请求，判断如果是在网友评论页面，则再次隐藏DETAIL
							var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-file-text-alt");
							if (nowPage) {
								$('#comment-list').parent().hide();
								$('.newsList').show();
							} else {
								$('.scrollContent').find('.newsList').hide();
								$('#comment-list').parent().show();
							}


							//保存未登陆时的detail缓存
							// var NoLandingCache = data.id;
							// Piece.Session.saveObject(NoLandingCache, data);
						}, null, null);
					}

				} else {
					// 登陆时保存不同的缓存
					// ----判断是否有登陆时的缓存-----
					var detailID = Util.request("id");
					var SessionDetail = Piece.Session.loadObject("L" + detailID)

					if (SessionDetail != null) {
						var SessionID = SessionDetail.id;
					}
					// -------------------
					if (SessionDetail != null && SessionID == detailID) {
						// 拿缓存数据
						var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
						SessionDetail.body = Util.removeImg(SessionDetail.body);
						var newsDetailHtml = _.template(newsDetailTemplate, SessionDetail);
						$(".newsList").remove();
						// newsDetailHtml = Util.removeImg(newsDetailHtml);
						$(".scrollContent").append(newsDetailHtml);
						// $("img").remove();
						var favorite = SessionDetail.favorite;


						if (favorite === 1) {
							$(".tab-item").find('.collect').removeClass("icon-star-empty").addClass("icon-star");
							$('.icon-star').css("color", "#0882f0");
						}

						if (SessionDetail.commentCount != 0) {
							$('.commentImg').css("display", "block");
							$('.commentCount').html(SessionDetail.commentCount)
						}
						if (SessionDetail.commentCount >= 10) {
							$('.promptInformation').css("width", "16px")
						}
						if (SessionDetail.commentCount >= 100) {
							$('.promptInformation').css("width", "22px")
						}

					} else {
						// 否则，没有缓存就重新请求
						var accesstoken = token.access_token;
						Util.Ajax(OpenAPI.news_detail, "GET", {
							id: Util.request("id"),
							dataType: 'jsonp',
							access_token: accesstoken
						}, 'json', function(data, textStatus, jqXHR) {
							//返回的结果都要和onshow的时候存入store的tempID进行比较，如果ID一样的话才进行append
							tempID = Piece.Store.loadObject("tempID");
							if (data.id == tempID) {
								// alert(11);
								//
								// 保存登陆时的detail缓存
								var LandingCache = "L" + data.id;
								Piece.Session.saveObject(LandingCache, data);

								var dataTemplate = data;
								var newsDetailTemplate = $(me.el).find("#newsDetailTemplate").html();
								dataTemplate.body = Util.removeImg(dataTemplate.body);
								var newsDetailHtml = _.template(newsDetailTemplate, dataTemplate);
								$(".newsList").remove();
								// newsDetailHtml = me.getimgsrc(newsDetailHtml);
								$(".scrollContent").append(newsDetailHtml);
								// $("img").remove();
								var favorite = data.favorite;
								if (favorite === 1) {
									$(".tab-item").find('.collect').removeClass("icon-star-empty").addClass("icon-star");
									$('.icon-star').css("color", "#0882f0");
								}

								if (data.commentCount != 0) {
									$('.commentImg').css("display", "block");
									$('.commentCount').html(data.commentCount)

								}
								if (data.commentCount >= 10) {
									$('.promptInformation').css("width", "16px")
								}
								if (data.commentCount >= 100) {
									$('.promptInformation').css("width", "22px")
								}
								// 刷新时，重新请求，判断如果是在网友评论页面，则再次隐藏DETAIL
								var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-file-text-alt");
								if (nowPage) {
									$('#comment-list').parent().hide();
									$('.newsList').show();
								} else {
									$('.scrollContent').find('.newsList').hide();
									$('#comment-list').parent().show();
								}
								
							}

						}, null, null);
					}
					//--------------------------
				}

				//如果是在评论页面，那么详情hide
				var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-file-text-alt");
				if (nowPage) {
					$('#comment-list').parent().hide();
					$('.newsList').show();
				} else {
					$('.scrollContent').find('.newsList').hide();
					$('#comment-list').parent().show();
				}


			},
			loadDetailComment: function() {

				// 拿缓存中的cube-list-comment-list-params中的ID与新ID比较，判断是否拿缓存
				var id = Util.request("id");
				Util.loadDifferentCommentList(this, "comment-list", OpenAPI.comment_list, {
					'id': id,
					'catalog': Util.request("com"),
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, "-comment-list-params", id);


			},
			// 刷新按钮，点击后从服务器重新拿评论列表数据
			reCommentList: function() {
				// $('#comment-list').remove();
				var id = Util.request("id");
				Util.loadList(this, "comment-list", OpenAPI.comment_list, {
					'id': id,
					'catalog': Util.request("com"),
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, true);

				var nowPage = $('footer.footer-menu-detail').find("li.active").find('span').hasClass("icon-comment-alt");
				console.info(nowPage);
				if (nowPage) {
					// alert(11);
					$('.newsList').hide();
					$('#comment-list').parent().show();

					return;
				} else {
					// alert(22);

					$('#comment-list').parent().hide();
					$('.newsList').hide();

				}

			},
			// 刷新按钮，加载最新的评论数信息
			newCommentContent: function() {
				Util.Ajax(OpenAPI.news_detail, "GET", {
					id: Util.request("id"),
					dataType: 'jsonp',
				}, 'json', function(data, textStatus, jqXHR) {
					if (data.commentCount != 0) {
						$('.commentImg').css("display", "block");
						$('.commentCount').html(data.commentCount)
					}
					if (data.commentCount >= 10) {
						$('.promptInformation').css("width", "16px")
					}
					if (data.commentCount >= 100) {
						$('.promptInformation').css("width", "22px")
					}

				}, null, null);
			}


		}); //view define

	});