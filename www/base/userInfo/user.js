define(["text!../../base/userInfo/user.html", "zepto", "../../base/openapi", '../../base/util'],
	function(UserHtml, $, OpenAPI, Util) {
		var User = Backbone.View.extend({

			id: 'userInfoContent',

			initialize: function() {
				this.render();
			},

			render: function() {
				var me = this;

				$(window).unbind("resize", me.checkuserInfoContent);

				//定义屏幕伸缩重新计算宽度
				$(window).resize(function() {
					me.checkuserInfoContent();
				});

				this.el = UserHtml;
				$("body").append(this.el);

				this.resetPosition();

				$("#commonContentMasker").click(function() {
					me.hide();
					$(".down-list").removeClass("down-list-up");
					$(".down-list").addClass("down-list-down");
				});

			},

			checkuserInfoContent: function() {
				if ($("#userInfoContent").css("display") === "block") {
					me.resetPosition();
					$("#userInfoContent").show();
				}
			},

			resetPosition: function() {
				$("#userInfoContent").show();
				var left = ($(window).width() - $("#userInfoContent").width()) / 2;
				var top = ($(window).height() - $("#userInfoContent").height()) / 9;
				$("#userInfoContent").css({
					"position": "absolute",
					"top": top,
					"left": left,
					"display": 'none'
				});
			},

			show: function() {
				$("#userInfoContent").show();
				$('#commonContentMasker').show()

			},
			hide: function() {
				$("#userInfoContent").hide();
				$('#commonContentMasker').hide()

			}

		});
		return User;
	});