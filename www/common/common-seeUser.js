define(['text!../common/common-seeUser.html', '../base/userInfo/user', "../base/openapi", '../base/util', "../base/openapi", '../base/login/login', "../tweet/tweet-list", "../news/news-list", "../question/question-list", "../base/caretInsert/jquery.hammer.min"],
	function(viewTemplate, UserInfo, OpenAPI, Util, OpenAPI, Login, TweetList, NewsList, QuestionList) {
		return Piece.View.extend({
			login: null,
			tweetList: null,
			newsList: null,
			questionList: null,
			id: 'common_common-seeUser',
			userInfo: null,
			events: {
				"click .title": "testUser",
				"click .backBtn": "gobackBtn",
				"click .refreshBtn": "refreshNewsContent",
				"click .message": "goMessage",
				"click .tweet": "goTweet",
				"click .tweetList": "tweetDetail",
				"click .userBlog": "gouserBlog",
				"click .userActive": "gouserActive",
				"click .newsList": "goBlogDetail"


			},
			deleteBlog: function(el) {
				var me = this;
				var dialog = new Piece.Dialog({
					autoshow: false,
					target: 'body',
					title: '确定删除此博客？',
					content: ''
				}, {
					configs: [{
						title: '确认',
						eventName: 'ok'
					}, {
						title: '取消',
					}],
					ok: function() {
						//检查登陆
						var checkLogin = Util.checkLogin();
						if (checkLogin === false) {
							var login = new Login();
							login.show();
							return;
						}
						var that = me;
						var userToken = Piece.Store.loadObject("user_token");
						var accessToken = userToken.access_token;
						var $target = $(el.currentTarget);
						console.info($target);
						var id = $target.attr("data-id");
						// console.info(id);
						Util.Ajax(OpenAPI.user_blog_delete, "GET", {
							access_token: accessToken,
							id: id,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							if (data.error === "200") {
								new Piece.Toast("删除成功");
								// that.onShow();
								var idTemp = Util.request("id");
								var fromAuthor = Util.request("fromAuthor");
								var fromAuthorId = Util.request("fromAuthorId");
								Util.reloadPage('tweet/hot-list?reload=1&blog=1&id=' + idTemp + '&fromAuthor=' + fromAuthor + '&fromAuthorId=' + fromAuthorId);
							} else {
								new Piece.Toast(data.error_description);
							}
						}, null, null);
					}
				});

				dialog.show();
				$('.cube-dialog-screen').click(function() {
					dialog.hide();
				})

			},
			goBlogDetail: function(el) {
				//如果confirm出现，那么return，不进入博客。
				if(typeof $('.cube-dialog-screen').get(0) !='undefined'){
					return;
				};

				//记录从博客列表进行跳转
				var json = {
					time: new Date().getTime()
				};
				var url = window.location.href + "&blog=1";
				window.history.replaceState(json, "", url);

				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var fromType = 3;
				var checkDetail = "news/news-blog-detail";
				var com = 5;
				//判断返回  到我的空间或用户信息模块还是  不同的DETAUIL详情
				var from = Util.request("from");

				this.navigateModule("news/news-blog-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
					trigger: true
				});
			},
			tweetDetail: function(el) {
				tweetList = new TweetList();
				newsList = new NewsList();
				questionList = new QuestionList();

				var $target = $(el.currentTarget);
				var id = $target.attr("data-objectId");
				var objType = $target.attr("data-objType");
				var url = $target.attr("data-url");
				
				/*-------------------传到详情页面参数-----------------------*/
				var fromType; //1-软件；2-问答；3-博客；4-咨询；5-代码；7-翻译；
				var checkDetail;
				var com; //1-新闻；2-问答；3-动弹；5-博客；

				if (objType == 4 || objType == 16) {
					fromType = 4;
					checkDetail = "news/news-detail";
					com = 1;
					//判断返回  到我的空间还是  不同的DETAUIL详情
					from = null;
					newsList.navigate("news/news-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				} else if (objType == 100 || objType == 101) {
					tweetList.navigate("tweet/tweet-detail?id=" + id, {
						trigger: true
					});
				} else if (objType == 3 || objType == 18) {
					fromType = 3;
					checkDetail = "news/news-blog-detail";
					com = 5;
					//判断返回  到我的空间还是  不同的DETAUIL详情
					from = null;

					newsList.navigate("news/news-blog-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				} else if (objType == 2 || objType == 17) {
					fromType = 2;
					checkDetail = "question/question-detail";
					com = 2;
					from = null;

					questionList.navigate("question/question-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				}
				else if(objType == 19){
						var ref = window.open(url, '_blank', 'location=yes');
						return false;
				}
			},
			goMessage: function() {
				var userOp = Util.request("fromAuthor");
				var userId = Util.request("fromAuthorId");

				this.navigate("comment-message?userOp=" + userOp + "&userId=" + userId, {
					trigger: true
				});
			},
			goTweet: function() {
				var userOp = Util.request("fromAuthor");
				userOp = "@" + userOp;
				this.navigateModule("tweet/tweet-issue?userOp=" + userOp, {
					trigger: true
				});
			},

			refreshNewsContent: function() {
				this.userInfo();
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				userInfo = new UserInfo();
				login = new Login();

				//write your business logic here :)
				$(".relationLogin").click(function() {
					Util.checkLogin();
					var checkLogin = Util.checkLogin();
					if (checkLogin === false) {
						login.show();
					} else {
						var noRelation = $('.relation').html();
						if (noRelation === "加关注") {
							var token = Piece.Store.loadObject("user_token");
							var user_message = Piece.Store.loadObject("user_message");
							var accesstoken = token.access_token;
							var id = Util.request("fromAuthorId");
							Util.Ajax(OpenAPI.user_relation, "GET", {
								friend: id,
								relation: 1,
								access_token: accesstoken,
								dataType: 'jsonp'
							}, 'json', function(data, textStatus, jqXHR) {
								var yesORno = data.error;
								if (yesORno == 200) {
									new Piece.Toast('您已经对Ta添加关注');
									$('.relation').html("取消关注");


								} else {
									new Piece.Toast(data.error_description);
								}
							}, null);
						} else {
							var token = Piece.Store.loadObject("user_token");
							var user_message = Piece.Store.loadObject("user_message");
							var accesstoken = token.access_token;
							var id = Util.request("fromAuthorId");
							Util.Ajax(OpenAPI.user_relation, "GET", {
								friend: id,
								relation: 0,
								access_token: accesstoken,
								dataType: 'jsonp'
							}, 'json', function(data, textStatus, jqXHR) {
								var yesORno = data.error;
								if (yesORno == 200) {
									new Piece.Toast('您已经对Ta取消关注');
									$('.relation').html("加关注");

								} else {
									new Piece.Toast(data.error_description);
								}
							}, null);
						}



					}
				});

				if (Util.request("blog") == 1) {
					// alert(11);
					$(".userActive").removeClass("active");
					$(".userBlog").addClass("active");
					$('#my-comment-list').parent().hide();
					$('#my-commentBlog-list').parent().show();
				}
				this.userInfo();

				//绑定长按
				var me = this;
				me.longTouch(me);

			},
			userInfo: function() {
				var me = this;

				var fromAuthor = Util.request("fromAuthor");
				var fromAuthorId = Util.request("fromAuthorId");
				var token = Piece.Store.loadObject("user_token");

				if (token === null) {
					Util.Ajax(OpenAPI.user_information, "GET", {
						friend: fromAuthorId,
						friend_name: fromAuthor,
						dataType: 'jsonp'
					}, 'json', function(data, textStatus, jqXHR) {
						$('.common-user').html(data.name);
						//user Name
						$('.userName').html(data.name);
						//user Sex
						if (data.gender == 1) {
							$('.userSex').html("男");
						} else {
							$('.userSex').html("女");
						}
						//user Img
						console.info(data.portrait)
						if (data.portrait != "/img/portrait.gif") {
							$('.userImgContent').attr("src", data.portrait);

						} else {
							$('.userImgContent').attr("src", "../base/img/widget_dface.ing");

						}
						// user From
						$('.userFrom').html(data.province);
						// user City
						$('.userFromCity').html(data.city);
						//join Time
						$('.userJoin').html(data.joinTime.substr(0, 10));
						//platforms

						if (data.platforms.length === 0) {
							$('.userPlatform').html("<无>");
						} else {
							$('.userPlatform').html(data.platforms + "");
						}
						//expertise
						if (data.expertise.length === 0) {
							$('.userSpeciality').html("<无>");
						} else {
							$('.userSpeciality').html(data.expertise + "");
						}
						//LoginTime
						$('.userLoginTime').html(data.lastLoginTime.substr(0, 10));


						me.loadList();
						me.loadBlogList();
					}, null);
				} else {
					var userID = Piece.Store.loadObject("user_message");
					Util.Ajax(OpenAPI.user_information, "GET", {
						user: userID.id,
						friend: fromAuthorId,
						friend_name: fromAuthor,
						dataType: 'jsonp'
					}, 'json', function(data, textStatus, jqXHR) {
						$('.common-user').html(data.name);
						//user Name
						$('.userName').html(data.name);
						//user Sex
						if (data.gender == 1) {
							$('.userSex').html("男");
						} else {
							$('.userSex').html("女");
						}
						//user Img
						if (data.portrait != "/img/portrait.gif") {
							$('.userImgContent').attr("src", data.portrait);

						} else {
							$('.userImgContent').attr("src", "../base/img/widget_dface.ing");

						}
						// user From
						$('.userFrom').html(data.province);
						// user City
						$('.userFromCity').html(data.city);
						//join Time
						$('.userJoin').html(data.joinTime.substr(0, 10));
						//platforms
						if (data.platforms.length === 0) {
							$('.userPlatform').html("<无>");
						} else {
							$('.userPlatform').html(data.platforms + "");
						}
						//expertise
						if (data.expertise.length === 0) {
							$('.userSpeciality').html("<无>");
						} else {
							$('.userSpeciality').html(data.expertise + "");
						}
						//LoginTime
						$('.userLoginTime').html(data.lastLoginTime.substr(0, 10));
						//relation
						if (data.relation === 1) {
							$('.relation').html("");
							$('.relation').html("取消互粉");
						} else if (data.relation === 2) {
							$('.relation').html("");
							$('.relation').html("取消互粉");
						} else {
							$('.relation').html("");
							$('.relation').html("加关注");
						}

						me.loadList();
						me.loadBlogList();
					}, null);
				}
			},
			loadList: function() {
				var id = Util.request("fromAuthorId");
				var user = Util.request("fromAuthor");
				Util.loadDifferentUsersList(this, 'my-comment-list', OpenAPI.my_list, {
					'catalog': 4,
					'user': id,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, "my-comment-list", user);
			},
			loadBlogList: function() {
				var user = Util.request("fromAuthor");
				var id = Util.request("fromAuthorId");
				var reload = Util.request("reload") == 1 ? true : false;
				Util.loadDifferentUsersList(this, 'my-commentBlog-list', OpenAPI.user_dynmic_blog, {
					'authoruid': id,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, "my-commentBlog-list", user, reload);
			},
			testUser: function() {
				//如果登录框出现，那么无法点击
				if (typeof $('#loginContentMasker').get(0) !== 'undefined') {
					if ($('#loginContentMasker').get(0).style.display == 'block') {
						return;
					}

				}

				var isShow = $('.down-list').hasClass("down-list-up");
				if (isShow) {
					userInfo.hide();
					$(".down-list").removeClass("down-list-up");
					$(".down-list").addClass("down-list-down");
				} else {
					userInfo.show();
					$(".down-list").removeClass("down-list-down");
					$(".down-list").addClass("down-list-up");
				}


			},
			gobackBtn: function() {
				window.history.back();
			},
			gouserBlog: function() {
				$('#my-comment-list').parent().hide();
				$('#my-commentBlog-list').parent().show();
			},
			gouserActive: function() {
				$('#my-comment-list').parent().show();
				$('#my-commentBlog-list').parent().hide();

			},
			longTouch: function(me) {
				var hammertime = $(me.el).find("#my-commentBlog-list").hammer();
				// console.info(hammertime);
				// on elements in the toucharea, with a stopPropagation
				hammertime.on("hold", ".newsList", function(ev) {
					me.deleteBlog(ev);
				});

			}


		}); //view define

	});